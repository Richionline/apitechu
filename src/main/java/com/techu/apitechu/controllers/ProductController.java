package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.util.ArrayList;

@RestController
public class ProductController {
    static final String API_BASE_URL = "/apitechu/v1";

    // GET products
    @GetMapping(API_BASE_URL + "/products")
    public ArrayList<ProductModel> getProducts() {

        System.out.println("Enter in getProducts");

        return ApitechuApplication.productModels;
    }

    // GET product with ID
    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) { //The param set in the url

        System.out.println("Enter in getProductById");
        System.out.println("ID es " + id);

        ProductModel result = new ProductModel();

        // Looking for the product to show
        for (ProductModel product: ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }

        return result;
    }

    // POST new product
    @PostMapping(API_BASE_URL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) { //The params are setting in the body

        System.out.println("Enter in createProduct");
        System.out.println("The product id is " + newProduct.getId());
        System.out.println("The product desc is " + newProduct.getDesc());
        System.out.println("The product price is " + newProduct.getPrice());

        // Add the new product
        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    // PUT product with ID
    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product,
                                      @PathVariable String id) { //The params are setting in the body

        System.out.println("Enter in updateProduct");
        System.out.println("The ID in url to update is " + id);
        System.out.println("The product to modify is " + product.toString());

        // Search for the product to update
        for (ProductModel productInList: ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId()); // It is no necessary
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    // DELETE product with ID
    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {

        System.out.println("Enter in deleteProduct");
        System.out.println("ID product to delete is " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        // Search for the product
        for (ProductModel product: ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Product found!");
                foundProduct = true;
                result = product;
            }
        }

        if (foundProduct) {
            System.out.println("Deleting product...");
            ApitechuApplication.productModels.remove(result);
        } else {
            System.out.println("Product to delete doesn't found");
        }


        return result;

    }

    // PATCH product, only desc or/and price
    @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData,
                                     @PathVariable String id) {

        System.out.println("Enter in patchProduct");
        System.out.println("ID product to patch is " + id);
        System.out.println("Product to update: " + productData.toString());

        ProductModel result = new ProductModel();
        Boolean foundProduct = false;

        for (ProductModel productInList: ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Product to update found");
                foundProduct = true;

                if (productData.getDesc() != null) {
                    System.out.println("Updating desc field with value: " + productData.getDesc());
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() > 0) {
                    System.out.println("Updating price field with value: " + productData.getPrice());
                    productInList.setPrice(productData.getPrice());
                }

                result = productInList;
            }
        }

        if (!foundProduct) {
            System.out.println("Nothing to update!");
        }

        return result;

    }

}
