package com.techu.apitechu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hello index";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "Tech U") String nameToShow) {
        return String.format("Hello %s!!!", nameToShow);
    }

}
